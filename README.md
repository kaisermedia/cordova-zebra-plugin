# Cordova Zebra Label Print

## Contents

 - Installation
 
 
## Installation

Install the plugin by executing the following commands:

```
ionic cordova plugin add kaiser-cordova-zebra-print --save
```

```
npm install @kaiser-software/cordova-zebra-print --save
```

## Usage

```typescript

// Discover Zebra printers in local network
window.cordova.plugins.ZebraLabelPrint.discover(printers => {
    // Zebra printers that were found
});

// Print zpl message to specified printer 
window.cordova.plugins.ZebraLabelPrint.print(zpl: string, printer_address: string, printer_port: number, response => {
    // do something with the response of the printer (read result)
});

```

## Development Notes

 - When developing the plugin make sure to remove and add the plugin each time you make changes to the `plugin.xml` file.
 - If an error of the form `Undefined symbols for architecture` occurs, you will probably want to include the `ExternalAccessory.framework`.
 
