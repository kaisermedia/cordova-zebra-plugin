var exec = require('cordova/exec');

var PLUGIN_NAME = 'ZebraLabelPrint';

var ZebraLabelPrint = {
	discover: function (success, error) {
		exec(success, error, PLUGIN_NAME, 'discover', []);
	},
	print: function (zpl, printer_address, printer_port, success, error) {
		exec(success, error, PLUGIN_NAME, 'print', [zpl, printer_address, printer_port]);
	}
};

module.exports = ZebraLabelPrint;
