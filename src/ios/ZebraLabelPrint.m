#import "ZebraLabelPrint.h"
#import "NetworkDiscoverer.h"
#import "DiscoveredPrinter.h"
#import "TcpPrinterConnection.h"
#import "ZebraPrinterConnection.h"

#import <Cordova/CDVAvailability.h>

@implementation ZebraLabelPrint

- (void)pluginInitialize {
}

- (void)discover:(CDVInvokedUrlCommand *)command {
    
    [self.commandDelegate runInBackground:^{
        
        self.printers = [NetworkDiscoverer localBroadcast:nil];
        NSMutableArray *discoveredPrinters = [NSMutableArray array];

        dispatch_async(dispatch_get_main_queue(), ^{
            for (DiscoveredPrinter *d in self.printers) {
                NSLog(@"Found printer: %@", d.address);
                [discoveredPrinters addObject:[NSString stringWithString:d.address]];
            }

            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:discoveredPrinters];
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        });
        
    }];
}

- (void)print:(CDVInvokedUrlCommand *)command {

    [self.commandDelegate runInBackground:^{
        
        NSString *zplData = [command.arguments objectAtIndex:0];
        NSString *printerIp = [command.arguments objectAtIndex:1];
        NSString *printerPort = [command.arguments objectAtIndex:2];

        // Instantiate connection for ZPL TCP port at given address.
        NSLog(@"Make connection");
        id<ZebraPrinterConnection, NSObject> thePrinterConn = [[TcpPrinterConnection alloc] initWithAddress:printerIp andWithPort:printerPort.intValue];
        [((TcpPrinterConnection*)thePrinterConn) setMaxTimeoutForOpen:3000];
        // Open the connection - physical connection is established here.
        BOOL success = [thePrinterConn open];

        NSError *error = nil;
        // Send the data to printer as a byte array.
        success = success && [thePrinterConn write:[zplData dataUsingEncoding:NSUTF8StringEncoding] error:&error];
        if (success != YES || error != nil) {
            NSLog(@"An error occurred!");
            
            [thePrinterConn close];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
            return;
        }

        [thePrinterConn waitForData:10000];

        NSData *read = [thePrinterConn read:&error];
        NSString *readData = [[NSString alloc] initWithData:read encoding:NSUTF8StringEncoding];

        NSLog(@"Read data: %@", readData);

        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:readData];
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];

        // Close the connection to release resources.
        [thePrinterConn close];

    }];
}

@end
