#import <Cordova/CDVPlugin.h>

@interface ZebraLabelPrint : CDVPlugin {
}

@property (nonatomic, strong) NSArray *printers;

// The hooks for our plugin commands
- (void)discover:(CDVInvokedUrlCommand *)command;
- (void)print:(CDVInvokedUrlCommand *)command;

@end
